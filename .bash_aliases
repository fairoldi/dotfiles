export GIT_PS1_SHOWDIRTYSTATE="true"
export GIT_PS1_SHOWDIRTYSTATE="true"
export GIT_PS1_SHOWUNTRACKEDFILES="true"
export GIT_PS1_SHOWUPSTREAM="auto"
export GIT_PS1_STATESEPARATOR="|"
export GIT_PS1_SHOWCOLORHINTS="true"

export PROMPT_DIRTRIM=2

export PS1='\[\e]0;\u@\h: \w\a\]\[\033[01;30m\][\A]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$(__git_ps1)\$ '

alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'

alias l='ls -F'
alias la='ls -aF'
alias ll='ls -laF'

set -o vi
